#!/bin/bash
if [ -z "$MATRIX_HOMESERVER" ]; then
	read -p "Enter homeserver URL: " MATRIX_HOMESERVER
fi
base_url="$MATRIX_HOMESERVER/_matrix/client/r0"
if [ -z "$MATRIX_ACCESS_TOKEN" ]; then
	read -p "Enter access token: " MATRIX_ACCESS_TOKEN
fi
auth="Authorization: Bearer $MATRIX_ACCESS_TOKEN"
resp=$(curl -s -H "$auth" "$base_url/account/whoami")
if [ $? -ne 0 ]; then
	echo "Failed to get account info. Invalid homeserver URL or access token? Response content:"
	echo "$resp"
	exit 1
fi
user=$(echo "$resp" | jq -r ".user_id")
if [ $? -ne 0 ]; then
	echo "Failed parse response when getting account info. Invalid homeserver URL or access token? Response content:"
	echo "$resp"
	exit 2
fi
echo "Logged in as $user"

if [ -z "$MATRIX_KICK_ROOM" ]; then
	read -p "Enter room ID or alias: " room_identifier
else
	room_identifier="$MATRIX_KICK_ROOM"
fi

echo "Joining $room_identifier..."
room_identifier=$(echo "$room_identifier" | sed 's/#/%23/')
resp=$(curl -s -H "$auth" -XPOST "$base_url/join/$room_identifier")
if [ $? -ne 0 ]; then
	echo "Failed to join $room_identifier. Response content:"
	echo "$resp"
	exit 3
fi
room_id=$(echo "$resp" | jq -r ".room_id")
if [ $? -ne 0 ]; then
	echo "Failed to parse response when joining $room_identifier. Response content:"
	echo "$resp"
	exit 4
fi

echo "Successfully joined $room_id, fetching member list..."
raw_members=$(curl -s -H "$auth" "$base_url/rooms/$room_id/joined_members")
if [ $? -ne 0 ]; then
	echo "Failed to get member list of $room_id. Response content:"
	echo "$raw_members"
	exit 5
fi

echo "Successfully fetched member list, parsing JSON..."
all_members=$(echo "$raw_members" | jq -r '.joined | keys[]')
if [ $? -ne 0 ]; then
	echo "Failed to parse response when getting member list of $room_id. Response content:"
	echo "$raw_members"
	exit 6
fi

dir="/tmp/matrix-kick-$(date +%s)"
mkdir $dir
echo "$all_members" > $dir/input
echo "Member list written to $dir/input"
echo "Update list as you want, write it to $dir/output and run "'`exit`'
echo 'to run kickbot, or run `exit 1` to cancel kickbot'
cd $dir && bash
if [ $? -ne 0 ]; then
	echo "Kicking cancelled"
	exit 7
elif [ ! -f $dir/output ]; then
	echo "Output file not found, kicking cancelled"
	exit 8
fi
members=$(cat $dir/output)
rm -rf $dir
echo -n "Starting kicking in 5 seconds"
for member in $members; do
	sleep 1.25
	echo -n "."
	sleep 1.25
	echo -n "."
	sleep 1.25
	echo "."
	sleep 1.25
	echo -n "Kicking $member, response: "
	curl -H "$auth" "$base_url/rooms/$room_id/kick" -d '{"user_id": "'"$member"'"}'
done
echo "All done!"